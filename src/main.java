import extensions.CSVFile;

class main extends Program {
    boolean []team = new boolean[4]; // tableau pour savoir si un personnage a fui ou pas
    int []fearLevel = new int[4]; // tableau pour savoir le niveau de peur d'un personnage
    String []inventaire = new String[4]; // l'inventaire ( 1 objet / personnage )
    String []objets = new String[3]; // les objets présents dans le jeu
    boolean []etatQuestion = new boolean[20]; // pour choisir l'une des 20 questions par difficulté et qu'elle ne puisse pas être posée deux fois
    String []name = new String [4]; // les prénoms de vos personnages
    int choix=0; // pour savoir qu'elle question a été choisie ( on aurait pu la déclarer dans un autre endroit )
    int nombreDeVivant = 4; // pour savoir combien de vos personnages sont en vies ( 4 de base )
    int difficulte=0; // la difficulté
    int pvBoss = 100;
    int pvPerso = 100;
    double probabilite=0; // la probabilité de devoir choisir entre un chemin A ou B

    CSVFile tableau =  loadCSV("ressources/tableau.csv");
    void algorithm() {
        menu();
    }

    void menu(){
        println("=============menu============");
        println("1. jouer");
        println("2. regles");
        println("3. quitter");
        println("choisissez ce que vous voulez");
        char utilisateur = readChar();
        while (utilisateur != '1' && utilisateur != '2' && utilisateur != '3'){
            println("vous vous êtes trompé, veulliez recommencer");
            utilisateur = readChar();
        }
        if(utilisateur == '1'){
            jouer();
            createTeam();
        }
        else if(utilisateur == '2'){
            regles();
        }else{}
    }


    void jouer() {
        lesObjets(objets);
        createTeam();
        setQuestion1();
        setName();
        choixDifficulte();
        println("");
        println("");
        println("");

        println("Dans un pays lointain se trouvait un village habité par de joyeux habitants.");
        println("Tout le monde dans ce village se connaissait et s’entraidait.");
        println("Malheureusement tout ne peut être parfait.");
        println("Non loin du village se trouvait un château, celui du Roi Doudou. On raconte que ce roi est endormi depuis très longtemps mais que s’il se réveille il apporterait chaos et désespoir a tous les habitants.");
        println("");
        println("Un jour, la porte du château s’ouvrit, c’était le signe que le roi doudou s’était réveillé ! ");
        println("Tout le monde était paniqué et ils décidèrent tous de déménager.");
        println("Seulement, un groupe de 4 enfants se montra plus courageux que tous les adultes. Ils décidèrent d’entrer dans ce château pour parler au roi.");
        println("Il ne savait pas que leur aventure allait être dangereuse et que le château était rempli de pièges.");
        println("Ces enfants ne sont autres que :"+name[0]+", "+name[1]+", "+name[2]+", "+name[3]+" !");
        delay(4000);
        lesObjets(objets);
        int nombreDeQuestionFalse = 0;
        while (team[0] && team[1] && team[2] && team[3] && nombreDeQuestionFalse < 10) {
            nombreDeQuestionFalse = 0;
            passage();
            question();
            for (int i = 0; i < 20; i++) {
                if (!etatQuestion[i]) {
                    nombreDeQuestionFalse++;
                    if (nombreDeQuestionFalse == 10) {
                        println("Vous avez gagné");
                    }
                }
            }
        }
        boss();
    }

    // voici toutes les fonctions qui ont pour but
    // - D'informer le joueur
    // - De choisir des variables obligatoires pour que le jeu marche exemple : la difficulté
    // - qui initialise des variables ou des tableaux exemple : l'inventaire

    void regles(){
        clearScreen();
        println("Nous allons vous expliquer les regles de ce jeu afin que vous puissiez jouer dans de bonnes conditions");
        println("1. Vous incarnez un groupe d'aventuriers, vous devez monter une tour afin de sauver une princesse" +
                " ( c'est pas vraiment une règle mais c'est mieux de connaître un minimum l'histoire )");
        println("2. Le Grand Méchant Qui Fait Peur a posé des pièges dans la tour afin de vous embêter, vous allez pouvoir" +
                " les desamorcés si vous repondez bien à une énigme");
        println("3. Vous avez un nombre de chance infini mais à chaque fois que vous répondez mal un membre de votre groupe" +
                " choisi aléatoirement gagnera 20 points de peur");
        println("4. Si l'un de vos personnage monte à 100 points de peur il sera testé");
        println("5. Si le personnage testé est <divin> alors il retombe à 0 de peur et vous pouvez vous estimer chanceux");
        println("5. Si le personnage testé est <déchu> alors il a peur et il s'enfuit de la tour, d'autres personnages pourraient " +
                "le suivre ...");
        println("6. Une dernière chose, n'oubliez pas qu'un groupe comptant peu d'invidu a plus facilement peur ...");
        println("en tout il y a au minimum 10 questions, bonne chance");
        println("");
        println("1. retourner au menu");
        println("2. quitter le jeu");
        char utilisateur1 = readChar();

        while(utilisateur1 != '1' && utilisateur1 != '2'){
            println("vous vous êtes trompé, veulliez recommencer");
            utilisateur1 = readChar();
        }
        if(utilisateur1 == '1'){
            algorithm();
        }else{}
    }

    void lesObjets(String []objets){
        println("Dans ce chateau il y a 3 types d'objets qui peuvent vous être utiles");

        println("1) Le doudou tout chou\n Il va pouvoir réconforter un de vos personnages, il va donc pouvoir baisser la " +
                "peur d'un de vos personnages");
        objets[0]="doudou tout chou";

        println("2) Le doud'audacieu  \n Il va pouvoir donner du courage un de vos personnages. " +
                "Le personnage qui l'a aura moins peur lorsque vous répondrez mal à une question");
        objets[1]="doud'audacieu";

        println("3) Le doud'hexorter  \n Il va pouvoir redonner du courage un de vos personnages qui a fui. " +
                "Vous pouvez l'utiliser pour que l'un de vos personnage qui a fui revienne");
        objets[2]="doud'hexorter";

        println("Voilà on a vu tout les objets que l'on va pouvoir (peut-être) avoir durant notre périple ! \n" +
                "Ah et aussi, vos personnages ne peuvent porter que un objet à la fois par soucis de place !");

        for (int i =1;i<length(inventaire);i++){
            inventaire[i]="rien";
        }
    }

    void choixDifficulte(){
        println("1. Facile");
        println("2. Intermédiaire");
        println("3. Difficile");
        char difficulteChoisie = readChar();
        while (difficulteChoisie != '1' && difficulteChoisie != '2' && difficulteChoisie != '3'){
            println("vous vous êtes trompé, veulliez recommencer");
            difficulteChoisie = readChar();
        }
        if(difficulteChoisie == '1'){
            difficulte=0;
        }
        else if(difficulteChoisie == '2'){
            difficulte=20;
        }else{
            difficulte=40;
        }
    }

    void setName(){
        for (int i=0; i<length(name); i++){
            println("comment voulez vous appeler le personnage n°"+i+" de votre groupe");
            name[i]=readString();
        }
    }


    void createTeam(){
        for (int i = 0; i<length(team);i++){
            team[i]=true;
        }
    }

    // voici toutes les fonctions qui ont un rapport avec les questions et le système de peur

    void setQuestion1(){
        for (int i = 0; i < length(etatQuestion);i++){
            etatQuestion[i]=true;
        }
    }

    void choixQuestion(){
        int nombre =(int)(random()*20);
        boolean questionDeBase = etatQuestion[nombre];
        while (questionDeBase == etatQuestion[nombre]) {
            if (etatQuestion[nombre]) {
                etatQuestion[nombre]=false;
                choix = nombre;
            }else{
                nombre =(int)(random()*20);
                questionDeBase = etatQuestion[nombre];
            }
        }
        println(getCell(tableau,nombre+difficulte,1));
    }

    String reponse(){
        return getCell(tableau,choix+difficulte,2);
    }

    void question(){
        choixQuestion();
        String reponseUtilisateur = readString();
        int firstLetterAnswer = (charAt(reponse(),0)+' ');
        println(reponse());
        if(charAt(reponseUtilisateur,0)==(char)firstLetterAnswer && length(reponseUtilisateur)==length(reponse())){
            reponseUtilisateur = reponse();
        }
        while (!(equals(reponseUtilisateur,reponse()))){
            if (reponseUtilisateur.equals("s")) {
                statsTeam();
                println("votre réponse ?");
                reponseUtilisateur = readString();
            }else if(equals(reponseUtilisateur,"i")){
                voirInventaire(inventaire);
                println("votre réponse ?");
                reponseUtilisateur = readString();
            } else {
                println("t'es naze");
                addRandomFear();
                if(!(fearLevel[0]<100 && fearLevel[1]<100 && fearLevel[2]<100 && fearLevel[3]<100)){
                    test(fearLevel[0],fearLevel[1],fearLevel[2],fearLevel[3]);
                }
                reponseUtilisateur = readString();
            }
        }
        println("bien joué beau gosse");
    }

    void passage(){
        double truc = random();
        if (probabilite>truc) {
            println("Votre groupe se retrouve bien embêté car devant eux se trouve deux portes.");
            println("Vous avez le choix entre prendre la porte de droite et celle de gauche");
            println("La première contient un piège plutôt simple");
            println("Par contre la deuxième a en son sein un piège plus complexe protégant un coffre ...");
            println("Laquelle choisissez vous, la porte de droite ou celle de gauche ?");

            String choix = readString();
            if (equals(choix, "droite")) {
                int tampon = difficulte;
                difficulte = 0;
                question();
                difficulte = tampon;
                probabilite = 0;
            } else if (equals(choix, "gauche")) {
                int tampon = difficulte;
                difficulte = 40;
                question();
                difficulte = tampon;
                println("Vous avez réussi une question très dur !");
                println("Votre groupe ouvre un coffre est découvre un magnifique objet !");
                objet(objets,inventaire);
                println("Votre groupe continue maintenant son chemin.");
                probabilite = 0;
            }
        }
        probabilite += 0.1;
        println(probabilite);
    }

    void addRandomFear(){
        int nombre =(int)(random()*4);
        int fearLevelDeBase = fearLevel[nombre];
        while (fearLevelDeBase == fearLevel[nombre]) {
            if (team[nombre] && doudaucieu(nombre,inventaire)) {
                fearLevel[nombre] += (20+difficulte)/2;
            }else if (team[nombre] && !doudaucieu(nombre,inventaire)){
                fearLevel[nombre] += (20+difficulte);
            } else{
                nombre =(int)(random()*4);
                fearLevelDeBase = fearLevel[nombre];
            }
        }
        println(name[nombre]+" a une peur de :" + fearLevel[nombre]+"/100");
    }

    void test(int personnage1,int personnage2,int personnage3,int personnage4) { // à revoir elle pourrait être moins longue
        if (personnage1 >= 100 && team[0]) {
            double nombre = random();
            if (nombre < 0.25) {
                team[0] = true;
                println("votre personnage est divin !");
                fearLevel[0]=0;
            } else {
                team[0] = false;
                println("votre personnage a pris peur et a fuis");
                inventaire[0]="rien";
                nombreDeVivant = nombreDeVivant - 1;
                test2(fearLevel[0],fearLevel[1],fearLevel[2],fearLevel[3]);
            }
        }
        if (personnage2 >= 100  && team[1]) {
            double nombre = random();
            if (nombre < 0.25) {
                team[1] = true;
                println("votre personnage est divin !");
                fearLevel[1]=0;
            } else {
                team[1] = false;
                println("votre personnage a pris peur et a fuis");
                inventaire[1]="rien";
                nombreDeVivant = nombreDeVivant - 1;
                test2(fearLevel[0],fearLevel[1],fearLevel[2],fearLevel[3]);
            }
        }
        if (personnage3 >= 100  && team[2]) {
            double nombre = random();
            if (nombre < 0.25) {
                team[2] = true;
                println("votre personnage est divin !");
                fearLevel[2]=0;
            } else {
                team[2] = false;
                println("votre personnage a pris peur et a fuis");
                inventaire[2]="rien";
                nombreDeVivant = nombreDeVivant - 1;
                test2(fearLevel[0],fearLevel[1],fearLevel[2],fearLevel[3]);
            }
        }
        if (personnage4 >= 100  && team[3]) {
            double nombre = random();
            if (nombre < 0.25) {
                team[3] = true;
                println("votre personnage est divin !");
                fearLevel[3]=0;
            } else {
                team[3] = false;
                println("votre personnage a pris peur et a fuis");
                inventaire[3]="rien";
                nombreDeVivant = nombreDeVivant - 1;
                test2(fearLevel[0],fearLevel[1],fearLevel[2],fearLevel[3]);
            }
        }
    }


    void test2(int personnage1,int personnage2,int personnage3,int personnage4) { // à revoir elle pourrait être moins longue
        if ((personnage1 == 80) && (nombreDeVivant > 1)) {
            double nombre = random();
            if (nombre < 0.5) {
                team[0] = false;
                println(name[0]+" l'a suivi");
                inventaire[0]="rien";
            } else {
                println(name[0]+" l'a pas suivi");
            }
        }
        if ((personnage2 == 80) && (nombreDeVivant > 1)){
            double nombre = random();
            if (nombre < 0.5) {
                team[1] = false;
                println(name[1]+" l'a suivi");
                inventaire[1]="rien";
            } else {
                println(name[1]+" l'a pas suivi");
            }
        }
        if ((personnage3 == 80) && (nombreDeVivant > 1)){
            double nombre = random();
            if (nombre < 0.5) {
                team[2] = false;
                println(name[2]+" l'a suivi");
                inventaire[2]="rien";
            } else {
                println(name[2]+" l'a pas suivi");
            }
        }
        if ((personnage4 == 80) && (nombreDeVivant > 1)){
            double nombre = random();
            if (nombre < 0.5) {
                team[3] = false;
                println(name[3]+" l'a suivi");
                inventaire[3]="rien";
            } else {
                println(name[3]+" ne l'a pas suivi");
            }

        }
    }
    void statsTeam(){
        for(int i = 0; i<length(team);i++){
            if (!team[i]){
                println(name[i] +" a eu peur et il a fuit comme un lâche");
            }else {
                println(name[i] + " a " + fearLevel[i] + " de peur, il est toujours en vie !");
            }
        }
    }

    void voirInventaire(String []inventaire){
        for (int i=0;i<length(inventaire);i++){
            println(name[i]+" possède "+inventaire[i]);
        }
        println("voulez vous utiliser un objet ?");
        if (equals(readString(),"oui")){
            println("quel personnage ?");
            String choix = readString();
            for (int i = 0;i<length(name);i++){
                if (equals(name[i],choix)){
                    utiliserUnObjet(i);
                }
            }
        }
    }

    // voici toutes les fonctions qui ont un rapport avec l'inventaire

    void objet(String []objets, String []inventaire){
        int alea = (int)(random()*3);
        println("Wow vous avez trouvé "+objets[alea]+" !");
        println("Sur quel perso voulez vous le mettre ? Vous avez 4 choix");
        for (int i = 0; i<length(name);i++){
            println(name[i]);
        }
        String choix = readString();
        for (int i = 0; i<length(name);i++){
            if (equals(choix,name[i])){
                inventaire[i]=objets[alea];
                println("Maintenant "+name[i]+" possède "+objets[alea]);
            }
        }
    }



    void utiliserUnObjet(int personnage){
        if (doudouToutChou(personnage,inventaire)){
            useDoudouToutChou();
        }else if (doudHexorter(personnage,inventaire)){
            useDoudHexorter();
        }else{
            println("Vous n'avez pas d'objet activable");
        }
    }

    boolean doudHexorter(int personnage,String []inventaire){
        if (equals(inventaire[personnage],"doud'hexorter")){
            return true;
        }else{
            return false;
        }
    }

    void useDoudHexorter(){
        int morts=0;
        String lesFuyards="\n";
        for (int i = 0;i<length(team);i++){
            if (!team[i]){
                morts++;
                lesFuyards+="-"+name[i]+"\n";
            }
        }
        if (morts>0){
            println("Vous avez la disponibilité de faire revenir quelqu'un\n" +
                    "voici la liste des fuyards"+lesFuyards);
            println("Qui voulez vous faire revenir ?");
            String choix = readString();
            boolean choixBon=false;
            int i = 0;
            while (!choixBon){
                if (i == length(name)){
                    println("vous vous êtes trompé !");
                    i=0;
                    choix = readString();
                }
                if (!team[i] && equals(choix,name[i])){
                    team[i]=true;
                    fearLevel[i]=0;
                    println(name[i]+" est revenue et en pleine forme !");
                    choixBon=true;
                }
                i++;
            }
        }
        if (morts==0){
            println("personne n'a fui");
        }
    }

    boolean doudaucieu(int personnage,String []inventaire){
        if (equals(inventaire[personnage],"doud'audacieu")){
            return true;
        }else{
            return false;
        }
    }

    boolean doudouToutChou(int personnage,String []inventaire){
        if (equals(inventaire[personnage],"doudou tout chou")){
            return true;
        }else{
            return false;
        }
    }

    void useDoudouToutChou(){
        println("Sur qui voulez vous utilisez votre Doudou tout chou ?");
        String choix = readString();
        boolean choixBon=false;
        int i = 0;
        while (!choixBon){
            if (i == length(name)){
                println("vous vous êtes trompé !");
                i=0;
                choix = readString();
            }
            if (team[i] && equals(choix,name[i])){
                fearLevel[i]=0;
                println(name[i]+" a repris sonn courage à deux mains !");
                choixBon=true;
            }
            i++;
        }
    }

    // voici toutes les fonctions qui ont un rapport avec le boss ( en construction )

    void boss(){
        /*int[]persClasse = new int[nombreDeVivant];
        for(int indice = 0;indice < nombreDeVivant;indice++){
            persClasse[indice] = indice;
        }*/
        println("Vous êtes arrivé au boss, faite de votre mieux !");
        while(pvBoss != 0 && pvPerso > 0){
            for(int pers = 1;pers <= nombreDeVivant;pers++){
                String classe = quelleClasse(pers);
                if(equals(classe, "mage")){
                    mageAttaque();
                } else if (equals(classe, "guerrier")){
                    guerrierAttaque();
                } else if (equals(classe, "tank")){
                    tankAttaque();
                }

            }
            if(pvBoss > 0){
                bossAttaque();
            }
        }
        if(pvBoss <= 0) {
            println("Le boss est mort !");
        } else if (pvPerso <= 0){
            println("Vous êtes mort");
        }
    }

    String quelleClasse(int perso){
        String classe = "";
        if(perso == 5){
            classe = "boss";
        } else
        if(perso == 1){
            classe = "mage";
        } else if(perso == 2){
            classe = "tank";
        } else {
            classe = "guerrier";
        }
        return classe;
    }

    void mageAttaque(){
        int sort1 = 0;
        println("Le mage attaque");
        println("1. Taper");
        choix = readInt();
        if (choix == 1) {
            pvBoss = pvBoss - sort1;
            println("Le boss perd "+ sort1 + " pv !");
        }
    }

    /*void choisirAttaque(String classe){
        int sort1 = 0;
        int sort2 = 0;
        int sort3 = 0;
        int choix = 0;
        if (equals(classe, "mage")) {
            println("Le mage attaque");
            println("1. Taper");
            sort1 = 5;
            choix = readInt();
            if (choix == 1) {
                degat = 5;
            }
        } else if(equals(classe, "tank")){
            println("Le tank attaque");
            println("1. Bouclier");
            sort1 = 5;
            choix = readInt();
            if(choix == 1){
                degat = 5;
            }
        } else if(equals(classe, "guerrier")){
            println("Le guerrier attaque");
            println("1. Epée");
            sort1 = 5;
            choix = readInt();
            if(choix == 1){
                degat = 5;
            }
        }
    }*/

    void bossAttaque(){
        println(" ");
        println("Le boss attaque");
        int attaqueBoss = 80 + (int)(random() * ((100 - 80) + 1));
        pvPerso = pvPerso - attaqueBoss;
        println("Ton equipe perd " + attaqueBoss + (" pv !"));
        println(" ");
    }

    void guerrierAttaque(){
        int sort1 = 0;
        println("Le guerrier attaque");
        println("1. Epée");
        choix = readInt();
        if(choix == 1){
            pvBoss = pvBoss - sort1;
            println("Le boss perd "+ sort1 + " pv !");
        }
    }

    void tankAttaque(){
        int sort1 = 5;
        println("Le tank attaque");
        println("1. Bouclier");
        choix = readInt();
        if(choix == 1){
            pvBoss = pvBoss - sort1;
            println("Le boss perd "+ sort1 + " pv !");
        }
    }

}